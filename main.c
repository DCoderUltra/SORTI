#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void check_input(int argc, char **argv, char ** fin);
void read_file_to_array(int ** array, int * size, char * fin);
void tsort(int * array, int size, int argc, char ** argv);
void merge_sort(int * array, int size);
void bubble_sort(int * array, int size);
int  sorted (int * array, int  size);
void print_file(int * array, int size, char * fout);
void help();

int main(int argc, char** argv){

  int size=0;
  int * array = malloc(sizeof(int));
  char * fin = malloc(sizeof(char));

  check_input(argc, argv, &fin);
  read_file_to_array(&array, &size, fin);

  tsort(array,size,argc,argv);

  free(array);
  free(fin);

  return 0;
}

void tsort(int * array, int size, int argc, char ** argv){
  double time;
  int algo = 0;
  if (argc == 3) sscanf(argv[2], "%d", & algo);
  clock_t t; 
  t = clock();
  switch(algo){
    case 0: merge_sort(array,size);
    case 2: bubble_sort(array,size);
            break;
    default: help(); exit(-1);
  }
  t = clock() - t;
  time = ((double)t)/CLOCKS_PER_SEC;

  if (sorted(array,size)){
    fprintf(stderr, "Numbers sorted: %d\n",size); 
    fprintf(stderr, "Time: %lf sec\n",time); 
    print_file(array, size, "sorted.txt");
  }
  
}

