#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help();

void check_input(int argc, char ** argv, char ** fin){
  if(argc < 2){
    help();
    exit(-1);
  }
  *fin = (char *) realloc(*fin, strlen(argv[1])+1);
  memcpy(*fin,argv[1], (size_t) strlen(argv[1])+1);
}

void read_file_to_array(int ** array, int * size, char * fin){

  FILE * fp = fopen(fin,"r");
  if (fp==NULL) {
    fprintf(stderr, "Wasn't possible to open the file: %s\n", fin);
    exit(-1);
  }

  int buffer = 0;
  while((fscanf(fp,"%d", &buffer))!=EOF){
    (*size)= (*size) + 1;
  }
  rewind(fp);

  *array = (int *) realloc(*array, (*size)*sizeof(int));
      
  for(int k=0; k < *size; k++){
    fscanf(fp,"%d", &(*array)[k]);
  }

  fclose(fp);
}
