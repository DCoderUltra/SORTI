#include <stdio.h>
#include <stdlib.h>

void print_a (int * array, int size){
  for( int i=0; i<size; i++){
    printf("[%d] = %d\n", i, array[i]);
  }
  putchar('\n');
}

void print_file(int * array, int size, char * fout){
  FILE * fp = fopen(fout,"w");
  if (fp==NULL) { 
    fprintf(stderr, "Wasn't possible to write to %s", fout); 
    exit(0); 
  }
  for(int k=0; k < size; k++){
    fprintf(fp,"%d\n", array[k]);
  }
  fclose(fp);
}

void help(){
  FILE * fp = fopen("help.txt", "r");
  if (fp == NULL){ 
    exit(-1);
    fprintf(stderr, "No help could be displayed check if you have the file help.txt");
  }
  int ch;
  while((ch=fgetc(fp))!=EOF){
    putchar(ch);
  }
}
