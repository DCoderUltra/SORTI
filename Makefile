PROG = Sorti
FILES = main.c sorting.c output.c input.c
COMP = gcc
FLAGS= -lm -Wall -O4

${PROG} : ${FILES}
	  ${COMP} -o ${PROG} ${FILES} ${FLAGS}
debug:
	  ${COMP} -o ${PROG} ${FILES} ${FLAGS} -g
	  gdb -q --tui ./${PROG}
