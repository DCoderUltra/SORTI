#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void merge_sort(int * array, int size){

  int n_steps = log2(size) + 1;
  int elem    = pow(2,n_steps);
  int * tmp   = malloc(elem*sizeof(int));  // Memory Alocation for a temporary array

  int i,i_e,j,j_e,k;

                                        
  for(int n=0; n<n_steps; n++){
    k=0;
    memcpy(tmp, array, sizeof(int)*size); 
    for(int s=0; s<elem/pow(2,n+1); s++){
      i   = s*pow(2,n+1); 
      i_e = i+pow(2,n);
      j   = i+pow(2,n);
      j_e = j+pow(2,n);

      while(i < i_e && j < j_e && j < size){
        if (tmp[i] < tmp[j]) 
          array[k++]= tmp[i++];
        else
          array[k++]= tmp[j++];
      }
      while (j<j_e && j<size) array[k++] = tmp[j++]; 
      while (i<i_e && i<size) array[k++] = tmp[i++];
    }
  }
  free(tmp);
}

void bubble_sort(int * array, int size){ 
  int a,b,c;
  for(a = 0; a < size; a++){       
    for(b = 0; b < size; b++){          
      if(array[b] > array[b+1]){               
        c = array[b];
        array[b] = array[b+1];
        array[b+1] = c;
            }
        }
    }
}

int sorted(int * array, int size){
    for(int k=0; k<size-1; k++){
      if(array[k]>array[k+1]){
        fprintf(stderr, "File not sorted at line: %d\n", k); 
        return 0;
      }
    }
    return 1;
}
